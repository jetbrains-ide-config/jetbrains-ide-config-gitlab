# UPDATE 2023-07

Some things have changed in how JetBrains handles config sync, so of the info below in this page may be outdated.

Also, there is now a [handbook section on JetBrains IDEs](https://handbook.gitlab.com/handbook/tools-and-tips/editors-and-ides/jetbrains-ides/),
 which should eventually supersede the info in this README, especially the [Setup and Config](https://handbook.gitlab.com/handbook/tools-and-tips/editors-and-ides/jetbrains-ides/setup-and-config/) subsection.

# jetbrains-ide-config-gitlab

Instructions for IDE-level and Project-level config for RubyMine for the [GitLab Product](https://gitlab.com/gitlab-org). There's two parts:

1. Instructions for setting up and managing IDE-level config via [Settings Repository plugin](https://plugins.jetbrains.com/plugin/7566-settings-repository/). The actual repository is in a [separate repo](https://gitlab.com/jetbrains-ide-config/jetbrains-ide-config-rubymine-settings-repository). See details below.
2. Instructions for managing project-level RubyMine `.idea` folder config.  This lives in this repo as the `dotidea` folder, and must be manually committed. See details below.

# Goals

* When setting up a new machine or multiple machines with Jetbrains IDEs, you shouldn't have to manually configure everything to work properly with the GitLab codebase and adhere to all GitLab coding standards and guidelines. Instead, you should be able to just check out the config from source control and use it.
* Also, if you somehow mess up your IDE config, it should all be under source control with small incremental commits, so you can see what changed and revert it if needed.

# Status

## Latest updates

* After using this for about six months, it seems to work fine. I have also used it to migrate settings to a new workstation.
* I just have to manually check in *.iml file every time the gems and rake tasks change. But I do that from the IDE itself (even though I usually do all my git commits from the command line), so it's just a couple of clicks to commit and push with the same message every time.  However, it would be best if these were pulled out of the `.iml` file.
  * TODO: open an issue for this.
* IMPORTANT NOTE: this may not restore all settings to a new workstation! Even if you use this approach, you should still go through the [manual setup for Jetbrains Overridden Settings](https://github.com/thewoolleyman/workstation/blob/master/README.md#jetbrains-overridden-settings) to ensure all settings are restored.
  * Also, ensure all plugins are installed first, so their configuration pages exist.
* One gotcha seems to be that it wants to save every time I change my find in project settings.
  * Issue for this one: https://youtrack.jetbrains.com/issue/IDEA-178601#focus=streamItem-27-3881049.0-0
* Also, its definitely a one-person thing. I dont think people could share it without getting conflicts, but anyone should be able to use the same process with their own repos.

## Overview

This setup is experimental, especially the usage of the Settings Repository plugin.
* I ([Chad](https://gitlab.com/cwoolley-gitlab)) have used the approach of committing and sharing the `.idea` Project-level config successfully for years on other teams.  It works pretty well, as long as you `gitignore` your user-specific `workspace.xml` (and maybe dictionary files). However, this was always committed directly to the root of the projects repo. Since that's not possible with the open source `gitlab` repo, we have to symlink it in.  But, it should be stable, you just have to remember to commit separately.
* However, this is the first time I've used the Settings Repository plugin to manage IDE-level config. It seems to be working well so far, though. I don't know how it will work when shared across multiple team members.
* If you want to try it out, let me know! There is also an GitLab internal slack channel `#jetbrains-ide`.


# Setup

## IDE-Level Config

* Follow the [instructions to set up a settings repository](https://www.jetbrains.com/help/idea/sharing-your-ide-settings.html#settings-repository) and point it to the [jetbrains-ide-config/jetbrains-ide-config-rubymine-settings-repository](https://gitlab.com/jetbrains-ide-config/jetbrains-ide-config-rubymine-settings-repository) repo, using the URL: `https://gitlab.com/jetbrains-ide-config/jetbrains-ide-config-rubymine-settings-repository.git`.
* Restart RubyMine. It should prompt for your GitLab credentials, enter your email and password. If you wish, you can create a new dedicated GitLab role account user which only has access to this repo.
  * UPDATE: apparently not needed, it automatically prompts for creds on restart: ~~You will need to set up [Caching your GitHub password in Git](https://help.github.com/en/github/using-git/caching-your-github-password-in-git) via `git-credential-manager-core`. You should create a new dedicated GitLab role account user which only has access to this repo, not your main GitLab account, to avoid caching your main account password in the OSX keychain.  (TODO: not sure if this is actually used?  All commits are under primary account's email?)~~
* The repo will be at:
  * ~~`/Users/<username>/Library/Preferences/RubyMine<VERSION>/settingsRepository/repository`~~ (old location)
  * `/Users/<username>/Library/Application Support/JetBrains/RubyMine<VERSION>/settingsRepository/repository`
* IMPORTANT NOTES:
  * Disable the plugin `IDE Settings Sync` (It conflicts with the `Settings Repository` plugin)
  * Ensure that pushing to the `master` branch is enabled for the repo. In GitLab Project Settings -> Repository -> Protected Branches -> delete `master`

### Pushing IDE-Level Config

There's multiple ways to push IDE-Level config changes to the settings repository:

1. Just exit the IDE.  It should sync automatically. (recommended if you exit IDE regularly)
1. Via the menu `VCS -> Sync Settings -> Merge` (recommended if you leave IDE open for a long time)
1. Commit and push normally via command line or IDE (not recommended, above are easier).  If you want to push via IDE, you'll need to use Credentials Helper, and ensure you have set up [Caching your GitHub password in Git](https://help.github.com/en/github/using-git/caching-your-github-password-in-git) as described above.

Also, note that if you manually change (e.g. revert) commits in the Settings Repository, you'll need to restart the IDE for these changes to show up.

## Project-Level Config

* Clone this repo (`git@gitlab.com:jetbrains-ide-config/jetbrains-ide-config-gitlab.git`) to your workspace directory, next to your `gitlab-development-kit` directory.
* Symlink the `dotidea` folder into your `gitlab` repo root as `.idea`:

```
cd gitlab-development-kit
ln -s ../../jetbrains-ide-config-gitlab/dotidea gitlab/.idea
```

* This repo must be manually committed when changes are made, and the .gitignore should be maintained as necessary to only commit appropriate files.  The easiest way to remember to do this is to open and "attach" it to the same IDE project window that you have the `gitlab` repo opened in.

# Process

## Periodic updates

* Whenever you pull new changes from upstream, or make config changes to the IDE, you may get changes to one or both of these repos.
* For the Project-level (`.idea`/`dotidea`) changes, you need to manually commit these right away, with an appropriate commit message.  This allows you to see what changed and revert if necessary. For ease of use, you can do this via the IDE (see below).
* For the IDE-level (`repository`) changes, these SHOULD be auto-synced periodically or when you exit the IDE, but you can sync them manually if you choose.  See details above in **"Pushing IDE-Level Config"**.

## Add Repos as Projects to IDE Workspace

In order to keep track of what's changed, and to easily commit changes to the `jetbrains-ide-config-gitlab` repo's `dotidea` folder, you can add them as projects to your workspace.

1. Open the `gitlab` project in Rubymine
1. Open `/Users/<username>/Library/Preferences/RubyMine<VERSION>/settingsRepository/repository` in the IDE and `Attach` it to the current window.
1. Open the `jetbrains-ide-config-gitlab` clone in the IDE and `Attach` it to the current window.

# Original Source of Config

The original config/settings in these repos was manually configured based on [thewoolleyman's workstation setup notes](https://github.com/thewoolleyman/workstation/blob/master/README.md#jetbrains-overridden-settings), with changes to comply with GitLab's [`.editorconfig`](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/.editorconfig)` and [development docs](https://docs.gitlab.com/ee/development/README.html).
